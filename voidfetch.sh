#!/usr/bin/env sh
# Filename: voidfetch.sh
# Function: Fetch basic system information on Void Linux
# by: tcincere [https://gitlab.com/tcincere] 

# Colours
esc="$(printf  "\033")"
red="${esc}[1;31m"
# green="${esc}[1;32m"
yellow="${esc}[1;33m"
# blue="${esc}[1;34m"
purple="${esc}[1:35m"
cyan="${esc}[1;36m"
white="${esc}[1;37m"
reset="${esc}[0m"


which_cat="cat"

# Typical system information
user=$(whoami)
distro=$(awk -F "=" 'NR == 1 {gsub("\"",""); print tolower($2)}' /etc/os-release)
host="$(uname -n)"
kernel="$(uname -r)"
packages=$(xbps-query -l | wc -l)
nix_packages=$(nix-env -q | wc -l)
shell="$(grep "$user" /etc/passwd | cut -d ":" -f 7)"

# Memory
memory_total=$(vmstat -s | head -n 2 | awk '$3 == "total" {print $1; exit(0)}' | numfmt --from-unit=K --to=iec)
memory_use=$(vmstat -s | head -n 2 | awk '$3 == "used" {print $1; exit(0)}' | numfmt --from-unit=K --to=iec)

uptime=$(uptime -p | sed 's/up //')

printNonPOSIX() 
{
		if [ -n "$file_font" ]; then
				useCustomFile
		fi

		if ! [ -f /usr/share/figlet/Whimsy.flf ] && [ -z "$file_font" ] ; then
				echo "error"
				exit 1
		fi


		if [ -z "$file_font" ]; then
				figlet -f "Calvin S" "v o i d" -t | lolcat
		fi
}

useCustomFile()
{
		if [ -z "$file_font" ]; then
			exit 1
		fi

		figlet -f "$file_font" "Void" -t | lolcat 
}

printHelp()
{
		cat <<EOF

		${cyan}
		Usage: ./$(basename "$0") [OPTS]

		${purple} Optional arguments: ${white}

		-h  print help and quit,
		-n  use non-posix variant,
		-f  use custom file font (.flf)

		${yellow} Note: ${red}Both -n and -f require figlet and lolcat!

		${reset}	
EOF
	
}


# Argument parsing
while getopts ":nf:" ARG; do
		case "$ARG" in
			n)
			 	printNonPOSIX
				which_cat="lolcat"
				;;
			f) 	
				file_font="$OPTARG"
				;;
			h)
				printHelp
				exit 0
				;;
			?)
				printHelp
				exit 2
				;;
		esac
done
shift $((OPTIND-1))


# Information and header colours.
info_type="${reset}${purple}"
info_info="${reset}${white}"
arrow="${reset}${cyan}->"


"$which_cat" <<EOF

${white} voidfetch
${info_type} user      ${arrow}  ${info_info}${user} (@${host}) 
${info_type} distro    ${arrow}  ${info_info}${distro} 
${info_type} kernel    ${arrow}  ${info_info}${kernel} 
${info_type} pkgs      ${arrow}  ${info_info}${packages} (xbps), ${nix_packages} (nix)
${info_type} memory    ${arrow}  ${info_info}${memory_use}/${memory_total} 
${info_type} shell     ${arrow}  ${info_info}${shell}
${info_type} uptime    ${arrow}  ${info_info}${uptime} 

EOF

